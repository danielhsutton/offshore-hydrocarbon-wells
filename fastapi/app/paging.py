import db_controller as db
import math


def get_page(offset, limit=1000, total=None, items=None, url=None):
    if offset == None:
        offset = 0
    paging = {}
    paging.update({"next": str(url) + f'?offset={offset + limit}'})
    if offset != 0:
        if offset - limit < 0:
            paging.update({"previous": str(url)})
        else:
            paging.update({"previous": str(url) + f'?offset={offset - limit}'})

    # if items:
    #     total = len(items)
    #     items = items[start:start+limit]

    # paging.update({"total": total})
    paging.update({"limit": limit})

    result = {"paging": paging,
              "items": items
              }

    return result
