from fastapi import FastAPI
from config import PROJECT_NAME
import routes


def get_app():
    app = FastAPI(title=PROJECT_NAME)
    app.include_router(routes.router)
    return app


app = get_app()
