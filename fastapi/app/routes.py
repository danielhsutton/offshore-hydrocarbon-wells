from fastapi import APIRouter
from models import Well, CoreRun, Paging, Image, Response, ItemSummary, ItemDetail, ItemResponse
import db_controller as db
import datetime
import paging
from starlette.requests import Request
import json

router = APIRouter()


@router.get("/")
def root():
    return {"Hello": "World"}


@router.get("/items",
            response_model=ItemResponse,
            responses={
                200: {
                    "description": "Return json of a Item"
                }
            },
)
def get_items(request: Request = None, q: str = None, start_date: datetime.date = None,
              end_date: datetime.date = None, bbox: str = None, dict: str = None, start: int = None):

    params = {}
    if q:
        params.update({"q": q})
    if start_date:
        params.update({"start_date": start_date})
    if end_date:
        params.update({"end_date": end_date})
    if bbox:
        bbox = [float(i) for i in bbox.split(",")]
        params.update({"bbox": bbox})
    if dict:
        dict = json.loads(dict)
        params.update({"dict": dict})

    results = {"rel": {
                        "text/html": None,
                        "self": request.url.path
                        },
                }

    item = {
        "item_id": 1,
        "name": "item",
        "description": "item",
        "date": "2018-01-02",
        "datetime": "2018-01-02T00:00:00.0000",
        "dictionary_object": {
                                "code": "AAA",
                                "description": "DESC",
                                "translation": "TRANSLATION",
                                },
        "bbox": [-23.95,55.11,-5.04,60.15],
        "feature_collection": [],
        "point": [10,10],
    }
    items = [item, item, item]

    paging_results = {}
    url = "127.0.0.1:8000/items"
    if start == None:
        paging_results = paging.get_page(items=items, url=url)
    else:
        paging_results = paging.get_page(start=start, items=items, url=url)
    results.update(paging_results)

    return results


@router.get(
    "/items/{item_id}",
    response_model=ItemSummary,
    responses={
            200: {
                "description": "Return json of item"
            }
        },
)
def get_item(item_id: int):
    # item = retrieve item info
    return {"item_id": item_id}


@router.get(
    "/wells",
    # response_model = Response,
    responses = {
             200: {
                 "description": "Return json of a Well"
             }
         },
)
def get_wells(request: Request = None, well_name: str=None, operator:str=None,
              top_depth:int=None, bottom_depth:int=None, units:str=None, sample_type:str=None,
              offset:int=None):

    params = {}
    if well_name:
        params.update({"well_name": well_name})
    if operator:
        params.update({"operator": operator})
    if top_depth:
        params.update({"top_depth": top_depth})
    if bottom_depth:
        params.update({"bottom_depth": bottom_depth})
    if units:
        params.update({"units": units})
    if sample_type:
        params.update({"sample_type": sample_type})
    # if offset:
    #     params.update({"offset": offset})

    results = {"rel": {
                        "text/html": None,
                        "self": request.url.path
                    },
               }

    wells = None
    paging_results = {}
    url = "127.0.0.1:8000/wells"
    wells = db.get_wells(conn=db.create_sqlite3_connection(), offset=offset, **params)
    paging_results = paging.get_page(offset=offset, items=wells, url=url)

    for well in paging_results.get("items"):
        samples = db.get_well_samples(db.create_sqlite3_connection(), well.get('WELLID'))
        well['CORE_RUNS'] = []
        for sample in samples:
            sample['IMAGE'] = None
            if sample.get('MATERIAL_TYPE') == well.get('MATERIAL_TYPE'):
                if sample.get('COREBOX_IMAGE_ID'):
                    sample['IMAGE'] = db.get_sample_image(db.create_sqlite3_connection(), sample.get('COREBOX_IMAGE_ID'))
                well['CORE_RUNS'].append(sample)

    results.update(paging_results)

    return results


@router.get(
    "/wells/{well_id}",
    # response_model=,
    responses={
            200: {
                "description": "Return json of well"
            }
        },
)
def get_well(well_id: int):

    well_items = db.get_well(conn=db.create_sqlite3_connection(), well_id=well_id)

    for well in well_items:
        samples = db.get_well_samples(db.create_sqlite3_connection(), well.get('WELLID'))
        well['CORE_RUNS'] = []
        for sample in samples:
            sample['IMAGE'] = None
            if sample.get('MATERIAL_TYPE') == well.get('MATERIAL_TYPE'):
                if sample.get('COREBOX_IMAGE_ID'):
                    sample['IMAGE'] = db.get_sample_image(db.create_sqlite3_connection(),
                                                          sample.get('COREBOX_IMAGE_ID'))
                well['CORE_RUNS'].append(sample)

    return well_items