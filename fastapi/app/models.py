from typing import List
from pydantic import BaseModel


class Paging(BaseModel):
    next: str
    previous: str = None
    total: int
    limit: int


class ItemDetail(BaseModel):
    code: str
    description: str
    translation: str


class ItemSummary(BaseModel):
    item_id: int
    name: str
    description: str
    date: str
    datetime: str
    dictionary_object: ItemDetail
    bbox: List[float]
    feature_collection: list
    point: List[float]


class ItemResponse(BaseModel):
    rel: dict = None
    paging: Paging = None
    items: List[ItemSummary] = None


class Image(BaseModel):
    FILE_PATH: str


# Material type/ sample type can be DRILLCORE, CUTTINGS or WASHED CUTTINGS
class CoreRun(BaseModel):
    MATERIAL_TYPE: str
    SET_NAME: str
    TOP_DEPTH: float
    BOTTOM_DEPTH: float
    IMAGE: Image = None


class Well(BaseModel):
    BOREHOLE_ID: int
    WELLID: int
    WELL_NAME: str
    OPERATOR: str
    LATITUDE: float
    LONGITUDE: float
    CORE_RUN_NO: int = None
    CORE_RUNS: List[CoreRun] = []
    LOCATION: str


class Relation(BaseModel):
    html: str
    self: str


class Response(BaseModel):
    rel: Relation = None
    paging: Paging = None
    items: List[Well] = None


