import os
import config
from etlhelper import connect, get_rows, DbParams, iter_rows
from etlhelper.row_factories import dict_rowfactory
import sqlite3
from sqlite3 import Error



os.environ['ORACLE_PASSWORD']=config.PASSWORD

dbparam = DbParams(
    dbtype='ORACLE',
    host='kwudb4.ad.nerc.ac.uk',
    port=1521,
    dbname='bgsdev',
    user=config.USERNAME)


def create_oracle_connection():
    return connect(dbparam, 'ORACLE_PASSWORD')


db_file = 'offshore.db'
def create_sqlite3_connection():
    """ create a database connection to a SQLite database """
    conn = None
    try:
        conn = sqlite3.connect(db_file, check_same_thread=False)
    except Error as e:
        print(e)

    return conn


def get_wells(conn, offset:int, n_rows:int=1000, **kwargs):
    """
       Return an Array of well rows from DB based on optional parameters. Builds a
       sql statement based on optional dictionary of query attributes and values.
       :param conn: database connection
       :param kwargs: dict of query parameters to be inserted into sql WHERE clause
       :return: Array of well rows
       """
    # sql = f'''
    #             SELECT * FROM (
    #                 SELECT BOREHOLE_ID, WELLID,  WELLNAME AS WELL_NAME, OPERATOR,
    #                 ORIG_SURFACE_LATITUDE AS LATITUDE, ORIG_SURFACE_LONGITUDE AS LONGITUDE,
    #                 CORE_RUN_NO, MATERIAL_TYPE AS sample_type,
    #                 TOP_DEPTH_M AS TOP_DEPTH, BOTTOM_DEPTH_M AS BOTTOM_DEPTH,
    #                 CURRENT_LOCATION AS LOCATION
    #                 FROM BGS.CSD_UKOFFSHOREHC_WELL_WEB
    #                 ORDER BY WELL_NAME)
    #             '''

    sql = f'''
                    SELECT * FROM (
                        SELECT BOREHOLE_ID, WELLID,  WELLNAME AS WELL_NAME, OPERATOR, 
                        ORIG_SURFACE_LATITUDE AS LATITUDE, ORIG_SURFACE_LONGITUDE AS LONGITUDE, 
                        CORE_RUN_NO, MATERIAL_TYPE,
                        TOP_DEPTH_M AS TOP_DEPTH, BOTTOM_DEPTH_M AS BOTTOM_DEPTH,
                        CURRENT_LOCATION AS LOCATION
                        FROM CSD_UKOFFSHOREHC_WELL_WEB
                        ORDER BY WELL_NAME)
                    '''

    sql_where = ""
    for k, v in kwargs.items():
        if k is not "start":
            if v:
                if len(sql_where) > 0:
                    sql_where += "AND"
                if k == 'top_depth':
                    sql_where += f' {k}>=\'{v}\' '
                elif k == 'bottom_depth':
                    sql_where += f' {k}<=\'{v}\' '
                elif k == 'well_name':
                    sql_where += f' {k} LIKE \'%{v}%\' '
                else:
                    sql_where += f' {k}=\'{v}\' '
    if len(sql_where) > 0:
        sql += "WHERE" + sql_where

    # offset pagination
    if offset == None:
        offset = 0
    sql += f'LIMIT {offset}, {n_rows}'

    return get_rows(sql, conn, row_factory=dict_rowfactory)


def get_well(conn, well_id):
    """
    Returns a Well object from bd given well ID
    :param conn: database connection
    :param well_id: Well ID integer of a given well object in db
    :return: Well object
    """
    sql = f'''
                SELECT BOREHOLE_ID, WELLID,  WELLNAME AS WELL_NAME, OPERATOR, 
                    ORIG_SURFACE_LATITUDE AS LATITUDE, ORIG_SURFACE_LONGITUDE AS LONGITUDE, 
                    CORE_RUN_NO, MATERIAL_TYPE AS sample_type,
                    TOP_DEPTH_M AS TOP_DEPTH, BOTTOM_DEPTH_M AS BOTTOM_DEPTH,
                    CURRENT_LOCATION AS LOCATION
                FROM CSD_UKOFFSHOREHC_WELL_WEB
                WHERE WELLID={well_id}
                ORDER BY TOP_DEPTH_M ASC
            '''

    return get_rows(sql, conn, row_factory=dict_rowfactory)


def get_well_samples(conn, well_id: int):
    """
        Return an Array of samples for a well
        :param conn: database connection
        :param well_id: Well ID integer of a given well object in db
        :return: Array of sample rows
           """
    sql = f'''
                SELECT MATERIAL_TYPE, SET_NAME, TOP_DEPTH, BOTTOM_DEPTH, 
                COREBOX_IMAGE_ID 
                FROM CSD_UKOFFSHOREHC_SAMPLE_WEB
                WHERE WELLID={well_id}
                ORDER BY TOP_DEPTH ASC
            '''

    return get_rows(sql, conn, row_factory=dict_rowfactory)


def get_all_samples(conn):
    sql = 'SELECT * FROM BGS.CSD_UKOFFSHOREHC_SAMPLE_WEB'
    return get_rows(sql, conn)


def get_image_idx(conn):
    sql = 'SELECT * FROM BGS.IMAGE_INDEX'
    return get_rows(sql, conn)


def get_sample_image(conn, corebox_img_id):
    sql = f'SELECT FILE_PATH FROM IMAGE_INDEX WHERE IMAGE_ID={corebox_img_id}'
    return get_rows(sql, conn, row_factory=dict_rowfactory)


def get_ext(conn):
    sql = 'SELECT * FROM BGS.BMD_EXTERNAL_CONFID'
    return get_rows(sql, conn)
