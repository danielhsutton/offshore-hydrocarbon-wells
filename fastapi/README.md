## SETUP

Copy config_template and rename config. Insert personal username and 
password variables for accessing BGS database. For more info see [etlhelper, DbParams section](https://pypi.org/project/etlhelper/).

```
pip install -r requirements.txt
```

cd into /app

```
uvicorn main:app
```

then go to: 127.0.0.1:8000 in browser


## API paths

* /docs for swagger UI view of paths and schemas

### Wells

* /wells for all wells - paging results in 10s
* /well?well_name=23/16f- 12 returns wells with names such as 23/16f- 12 and 23/16f- 12Z - also discriminator MATERIAL_TYPE[CUTTINGS or DRILLCORE]
* /wells?sample_type=CUTTINGS returns all wells with material_type CUTTINGS. Replace with DRILLCORE or WASHED CUTTINGS or leave blank for ALL
* /wells?top_depth=900 returns wells with top_depth >= to value
* /wells?bottom_depth returns wells with bottom_depth <= value
* /wells?units=ft returns wells with units in ft, m for meters
* /wells?operator=BP returns wells operated by BP

### Generic

For generic Items to get started on any API

* /items - all items
* /items?q=QUERY
* /items?bbox=1.33,1.22,19.99,-23.22 for bounding box with 4 floats
* /items?dict={"att":"value","att":"value"} for dictionary parameter
* /items?start_date=2017-01-01&end_date=2018-01-02 items with dates between specified dates


## Paging:

* /[resource]?start=1000 2nd batch of items